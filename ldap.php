<?php
//	Copyright (C) 2017-2018	Victor Bruna
//	
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
// 
//	LDAP search and export all users/objects to excel sheet while handling LDAP's limitation of only returning 1000 entries
// 	basic sequence with LDAP is connect, bind, search, interpret search
// 	result, close connection

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . './PHPExcel-1.8/Classes/PHPExcel.php';

echo "<h3>LDAP query test</h3>";
echo "Connecting ...";
$ds=ldap_connect("AD SERVER ADDRESS");  //must be a valid LDAP server!
echo "connect result is " . $ds . "<br />";

if ($ds) 
{ 
	ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
	ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
   echo "Binding ..."; 
   $ldapbind = @ldap_bind($ds, "USERNAME@DOMAIN.com", "DOMAIN PASSWORD" );

   // Search surname entry * and user is not disabled
   $dn="dc=DOMAIN,dc=COM";
   $filter="(&(objectClass=user)(objectCategory=person)(sn=*) )";//include (userAccountControl=512) - to return active users only
   $justtheseattributes = array( "ou", "cn", "sn", "givenname", "mail","distinguishedName","displayName","samaccountname");		
   
   
   // Create new PHPExcel object
	echo date('H:i:s') , " Create new PHPExcel object" , EOL;
	$objPHPExcel = new PHPExcel();

	// Set document properties
	echo date('H:i:s') , " Set document properties" , EOL;
	$objPHPExcel->getProperties()->setCreator("B. Victor")
								 ->setLastModifiedBy("B. Victor")
								 ->setTitle("LDAP Users")
								 ->setSubject("LDAP USers")
								 ->setDescription("LDAP Users - PHP")
								 ->setKeywords("office PHPExcel php")
								 ->setCategory("LDAP");
   
   
   /****COOKIE****/
	// enable pagination with a page size of 100.
	$pageSize = 1000;

	$cookie = '';
	$wrksheet = 0;
	do 
	{
		ldap_control_paged_result($ds, $pageSize, true, $cookie);
	 
		$sr=ldap_search($ds, $dn, $filter ,$justtheseattributes,0,0);  
		echo "Search result is " . print_r($sr) . "<br />";

		echo "Number of entires returned is " . ldap_count_entries($ds, $sr) . "<br />";
		echo "Worksheet: ". $wrksheet;
		echo "Getting entries ...<p>";
		$info = ldap_get_entries($ds, $sr);
		echo "Data for " . $info["count"] . " items returned:<p>";
		
		$objPHPExcel->createSheet();
		 // Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex($wrksheet);
		// Add some data
		echo date('H:i:s') , " Add some data" , EOL;
		//Add heading at the top of the page
		$objPHPExcel->getActiveSheet()->mergeCells('A1:C1')
				->setCellValue('A1','LDAP Report')
				->getStyle('A1')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		//make bold
		$objPHPExcel->getActiveSheet()->getStyle('A1')
				->applyFromArray(array("font" => array( "bold" => true)));  
		//entries
		$objPHPExcel->getActiveSheet()->mergeCells('A2:C2')
				->setCellValue('A2',"Data for " . $info["count"] . " items returned.")
				->getStyle('A2')
				->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		//make bold
		$objPHPExcel->getActiveSheet()->getStyle('A2')
				->applyFromArray(array("font" => array( "bold" => true)));
		//SET FILTER
		$objPHPExcel->getActivesheet()->setAutoFilter('A3:J3');

		//Headings
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Name')
				->getStyle('A3')
				->applyFromArray(array("font" => array( "bold" => true)));
				
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Dept')
				->getStyle('B3')
				->applyFromArray(array("font" => array( "bold" => true)));
				
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Email')
				->getStyle('C3')
				->applyFromArray(array("font" => array( "bold" => true)));
				
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Username')
				->getStyle('C3')
				->applyFromArray(array("font" => array( "bold" => true)));


		// Set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Century Gothic')->setSize(10);

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle("LDAP Report");
		$row = 4;
		foreach ($info as $e) 
		{
			//echo $e['dn'] . PHP_EOL;
			echo "dn is: " . $e["dn"] . "<br />";
		    echo "Name is: " . $e["cn"][0] . "<br />";
		   
		    $ln1 = strpos($e["dn"], 'OU');
		    $ln2 = strrpos($e["dn"], 'OU');
		    $dept = substr($e["dn"], $ln1 + 3, (($ln2-1)-($ln1 + 3)));
		    $sam=$e['samaccountname'][0];
		    echo "Dept is: " . $dept . "<br />";
		    echo "Username is: " . $sam . "<br />";
		    echo "Email is: " . $e["mail"][0] . "<br /><hr />";
			
			 //Excel
			   $objPHPExcel->getActiveSheet()
					->setCellValue('A'.$row, $e["cn"][0])
					->setCellValue('B'.$row, $dept)
					->setCellValue('C'.$row, $e["mail"][0])
					->setCellValue('D'.$row, $e["samaccountname"][0]);
				$row++;
		}
		
		
		//Auto size column widths
		$objPHPExcel->getActivesheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActivesheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActivesheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActivesheet()->getColumnDimension("D")->setAutoSize(true);

		ldap_control_paged_result_response($ds, $sr, $cookie);
		$wrksheet++;
	} while($cookie !== null && $cookie != '');
	/****COOKIE****/
   echo "Closing connection";
   ldap_close($ds);

} else {
   echo "<h4>Unable to connect to LDAP server</h4>";
}

// Save Excel 2007 file
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024), " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing files" , EOL;
echo 'Files have been created in ' , getcwd() , EOL;
  
?>